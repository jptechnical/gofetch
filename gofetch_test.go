package main

import (
	"fmt"
	"os"
	"reflect"
	"testing"
)

var filename = "packages.yml"
var sampleText = []string{"test1", "test2", "test2"}

func TestCreateTestFile(t *testing.T) {
	// Write a test file
	f, err := os.Create(filename)
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}

	for _, v := range sampleText {
		fmt.Fprintln(f, v)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

}

func TestBrewCheck(t *testing.T) {
	got := BrewCheck()
	want := "/usr/local/bin/brew"
	if got != want {
		t.Errorf("got %v want %v", got, want)
	}
}

func TestGetBrewList(t *testing.T) {
	got := GetBrewList()
	notwant := ""
	if got == notwant {
		t.Errorf("got %q notwant %q", got, notwant)
	}
}

func TestGetBrewPackage(t *testing.T) {
	assertCorrectMessage := func(t *testing.T, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}

	t.Run("Confirm package test exists", func(t *testing.T) {
		got := GetBrewPackage("foo", "foo")
		want := "foo"
		assertCorrectMessage(t, got, want)
	})

	t.Run("Send empty string for name", func(t *testing.T) {
		got := GetBrewPackage("foo", "")
		want := "Name or List is empty"
		assertCorrectMessage(t, got, want)
	})

	t.Run("Send space for name", func(t *testing.T) {
		got := GetBrewPackage("foo", " ")
		want := "Name or List is empty"
		assertCorrectMessage(t, got, want)
	})

	t.Run("Send empty string for list", func(t *testing.T) {
		got := GetBrewPackage("", "foo")
		want := "Name or List is empty"
		assertCorrectMessage(t, got, want)
	})

	t.Run("Search through string", func(t *testing.T) {
		got := GetBrewPackage("foo bar", "foo")
		want := "foo"
		assertCorrectMessage(t, got, want)
	})

	t.Run("Search non-existing string", func(t *testing.T) {
		got := GetBrewPackage("foo bar", "bash")
		want := "Package not found"
		assertCorrectMessage(t, got, want)
	})

	t.Run("Search for actual package installed", func(t *testing.T) {
		got := (GetBrewPackage(GetBrewList(), "z"))
		want := "z"
		assertCorrectMessage(t, got, want)
	})

	t.Run("Search for actual package missing", func(t *testing.T) {
		got := (GetBrewPackage(GetBrewList(), "foo"))
		want := "Package not found"
		assertCorrectMessage(t, got, want)
	})

}

func TestBrewPackageUpgrade(t *testing.T) {
	assertCorrectMessage := func(t *testing.T, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}

	t.Run("Send installed package", func(t *testing.T) {
		got := BrewPackageUpgrade("z")
		want := ""
		assertCorrectMessage(t, got, want)
	})

}

func TestLoadFileToVar(t *testing.T) {

	assertCorrectMessage := func(t *testing.T, got []string, want []string) {
		t.Helper()
		if reflect.DeepEqual(got, want) != true {
			t.Errorf("got %q want %q", got, want)
		}
	}

	t.Run("Send installed package", func(t *testing.T) {
		got := LoadFileToVar(filename)
		want := sampleText
		assertCorrectMessage(t, got, want)
	})

}

func TestDeleteTestFile(t *testing.T) {
	// Delete the test file
	e := os.Remove(filename)
	if e != nil {
		fmt.Println(e)
		return
	}
}
