package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

const testpkg = "hugo"

func main() {
	isBrewInstalled := BrewCheck()
	if isBrewInstalled == "/usr/local/bin/brew" {
		GetBrewList()
		fmt.Printf("Checking for package %v\n", testpkg)
		if (GetBrewPackage(GetBrewList(), testpkg)) == testpkg {
			fmt.Printf("Package %v installed\n", (GetBrewPackage(GetBrewList(), testpkg)))
		}
		if (GetBrewPackage(GetBrewList(), testpkg)) != testpkg {
			fmt.Printf("  %s... installing now\n", (GetBrewPackage(GetBrewList(), testpkg)))
			BrewPackageInstall(testpkg)
		}
		fmt.Println(BrewPackageUpgrade(testpkg))
	}
}

func BrewCheck() string {
	path, err := exec.LookPath("brew")
	if err != nil {
		log.Fatal("Homebrew must be installed, visit https://brew.sh")
		return fmt.Sprintf("%s", err)
	}
	return fmt.Sprintf(path)
}

func GetBrewList() string {
	output, err := exec.Command("sh", "-c", "/usr/local/bin/brew list -1").Output()
	if err != nil {
		log.Fatal(err)
	}
	list := string(output)                      // Convert bytes to string
	list = strings.Replace(list, "\n", " ", -1) // Change to space delimited
	list = strings.TrimRight(list, " ")         // Remove the trailing space
	return list
}

func GetBrewPackage(list string, name string) string {
	if (name == "") || (name == " ") || (list == "") {
		// I tried ...!= "") &&... but it won't compile
		return "Name or List is empty"
	}
	matched, _ := regexp.MatchString(name, list)
	switch matched {
	case true:
		return name
	case false:
		return "Package not found"
	default:
	}
	return "something didn't work"
}

func BrewPackageUpgrade(name string) string {
	output, err := exec.Command("/usr/local/bin/brew", "upgrade", name, "--quiet", "--dry-run").Output()
	if err != nil {
		log.Fatal(err)
		fmt.Printf("Error: %s ---\n Output: %s \n", err, output)
	}
	return ""
}

func BrewPackageInstall(name string) string {
	output, err := exec.Command("/usr/local/bin/brew", "install", name, "--quiet").CombinedOutput()
	if err != nil {
		log.Fatal(err)
		fmt.Printf("Error: %s ---\n Output: %s \n", err, output)
	}
	fmt.Printf("Install output: %q", output)
	return ""
}

func LoadFileToVar(filename string) []string {
	file, err := os.Open(filename)

	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var packageList []string

	for scanner.Scan() {
		packageList = append(packageList, scanner.Text())
	}

	file.Close()

	for _, eachline := range packageList {
		fmt.Println(eachline)
	}
	return packageList
}
